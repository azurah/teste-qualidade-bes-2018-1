package br.ucsal.testequalidade.bes20181.exemplo;

public class Fatorial {

	Long calcularFatorial(int n) {
		Long fat = 1L;
		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

}
